package eu.ismokilpelainen.android.footyapp.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.LevelListDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import eu.ismokilpelainen.android.footyapp.R;


public class TableHeaderView extends AppCompatTextView {

    private LevelListDrawable icon;
    private SortOrder order = SortOrder.NONE;

    public enum SortOrder {ASCENDING, DESCENDING, NONE}


    public TableHeaderView(Context context) {
        super(context);
        initView(context);
    }

    public TableHeaderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public TableHeaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        icon = (LevelListDrawable) ContextCompat.getDrawable(context, R.drawable.icon_sort);
        icon.setTint(Color.parseColor("white"));
        icon.setEnterFadeDuration(100);
        icon.setExitFadeDuration(100);
        /*initClickListeners();*/
    }

/*    private void initClickListeners() {
        this.setOnClickListener((v) -> {
                TextView textView = (TextView)v;
                LevelListDrawable d = (LevelListDrawable) textView.getCompoundDrawables()[2];
                if (d == null) {
                    Log.d("OMADEBUG","d was null");
                    icon.setLevel(0);
                    textView.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, icon, null);
                } else {
                    d.setLevel((d.getLevel()+1)%2);
                    Log.d("OMADEBUG","Level = " + d.getLevel());
                }
        });
    }*/

    public SortOrder getSortOrder() {
        return this.order;
    }

    public void setSortOrder(SortOrder order) {
        this.order = order;
        Log.d("OMADEBUG", "Setting sort order: " + order);

        switch (order) {

            case ASCENDING:
                icon.setLevel(1);
                setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, icon, null);
                break;
            case DESCENDING:
                icon.setLevel(0);
                setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, icon, null);
                break;
            case NONE:
                setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
                break;
        }
    }
}
