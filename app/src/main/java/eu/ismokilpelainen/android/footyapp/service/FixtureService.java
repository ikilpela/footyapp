package eu.ismokilpelainen.android.footyapp.service;

import eu.ismokilpelainen.android.footyapp.model.response.FixtureResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface FixtureService {

    @GET("/teams/{id}/fixtures")
    public Call<FixtureResponse> getTeamFixtures(@Path("id") int teamId);

    @GET("/competitions/{id}/fixtures")
    public Call<FixtureResponse> getCompetitionFixtures(@Path("id") int competitionId);

    @GET
    public Call<FixtureResponse> getFixtures(@Url String url);

}
