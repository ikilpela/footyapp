package eu.ismokilpelainen.android.footyapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashMap;

import eu.ismokilpelainen.android.footyapp.converter.LinksDeserializer;

/*@JsonIgnoreProperties(value={"_links"})*/
public class TeamStanding extends TeamRecord {

    @JsonProperty
    private int position;

    @JsonProperty("_links")
    @JsonDeserialize(using = LinksDeserializer.class)
    private HashMap<String, String> links;

    @JsonProperty
    private String teamName;
    @JsonProperty("crestURI")
    private String crestUri;
    @JsonProperty
    private int playedGames;
    @JsonProperty
    private int points;
    @JsonProperty
    private int goalDifference;
    @JsonProperty
    private TeamRecord home;
    @JsonProperty
    private TeamRecord away;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getCrestUri() {
        return crestUri;
    }

    public void setCrestUri(String crestUri) {
        this.crestUri = crestUri;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public void setPlayedGames(int playedGames) {
        this.playedGames = playedGames;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public int getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(int goalDifference) {
        this.goalDifference = goalDifference;
    }

    public TeamRecord getHome() {
        return home;
    }

    public void setHome(TeamRecord home) {
        this.home = home;
    }

    public TeamRecord getAway() {
        return away;
    }

    public void setAway(TeamRecord away) {
        this.away = away;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamLink() {
        return links.get("team");
    }
}
