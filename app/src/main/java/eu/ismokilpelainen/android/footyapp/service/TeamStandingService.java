package eu.ismokilpelainen.android.footyapp.service;

import eu.ismokilpelainen.android.footyapp.model.response.LeagueTableResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface TeamStandingService {

    /*@Headers({"X-Auth-Token: 9a6f9f1d61844b7c87590b7198a05bca", "X-Response-Control: full"})*/
    @GET("competitions/{id}/leagueTable")
    Call<LeagueTableResponse> getTeamStandings(@Path("id") int competitionId);
}
