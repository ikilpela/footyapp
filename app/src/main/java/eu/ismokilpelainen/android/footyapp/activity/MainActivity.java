package eu.ismokilpelainen.android.footyapp.activity;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eu.ismokilpelainen.android.footyapp.R;
import eu.ismokilpelainen.android.footyapp.adapter.StandingsAdapter;
import eu.ismokilpelainen.android.footyapp.decorator.DividerLineDecoration;
import eu.ismokilpelainen.android.footyapp.model.Competition;
import eu.ismokilpelainen.android.footyapp.model.response.LeagueTableResponse;
import eu.ismokilpelainen.android.footyapp.model.TeamStanding;
import eu.ismokilpelainen.android.footyapp.service.ApiService;
import eu.ismokilpelainen.android.footyapp.service.CompetitionService;
import eu.ismokilpelainen.android.footyapp.service.TeamStandingService;
import eu.ismokilpelainen.android.footyapp.view.TableHeaderView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_TEAM_URL = "eu.ismokilpelainen.android.footyapp.TEAMURL";

    private List<TeamStanding> standingsList = new ArrayList<>();
    private List<Competition> spinnerList = new ArrayList<>();

    private TextView leagueTitleView;
    private RecyclerView recyclerView;
    private Spinner competitionSpinner;
    private ProgressBar progressBar;

    private StandingsAdapter adapter;
    private ArrayAdapter<Competition> spinnerAdapter;

    TeamStandingService teamStandingService = ApiService.getInstance()
            .getService(TeamStandingService.class);
    CompetitionService competitionService = ApiService.getInstance()
            .getService(CompetitionService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int orientation = getResources().getConfiguration().orientation;

        RelativeLayout headers = findViewById(R.id.headerLayout);

        for (int i = 0; i < headers.getChildCount(); i++) {
            TableHeaderView v = (TableHeaderView) headers.getChildAt(i);
            v.setOnClickListener(this::sortByHeader);
        }


 /*       TableHeaderView posHeader, teamHeader, gpHeader, winsHeader, tiesHeader, lossesHeader,
                pointsHeader, goalsForHeader, goalsAgainstHeader,goalDifferenceHeader;

        posHeader = headers.findViewById(R.id.standing);

        teamHeader = headers.findViewById(R.id.teamName);

        gpHeader = headers.findViewById(R.id.gamesPlayed);
        winsHeader = headers.findViewById(R.id.wins);
        tiesHeader = headers.findViewById(R.id.ties);
        lossesHeader = headers.findViewById(R.id.losses);
        pointsHeader = headers.findViewById(R.id.points);

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            goalsForHeader = headers.findViewById(R.id.goalsFor);
            goalsAgainstHeader = headers.findViewById(R.id.goalsAgainst);
            goalDifferenceHeader = headers.findViewById(R.id.goalDifference);
        }*/

        // Init views
        leagueTitleView = findViewById(R.id.leagueTitle);
        recyclerView = findViewById(R.id.standingsView);
        competitionSpinner = findViewById(R.id.spinner);
        progressBar = findViewById(R.id.progressBar);

        //Init adapters
        adapter = new StandingsAdapter(standingsList, this);
        spinnerAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, spinnerList);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set competition dropdown adapter and add item selected listener
        competitionSpinner.setAdapter(spinnerAdapter);
        competitionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Competition c = (Competition) parent.getItemAtPosition(position);
                leagueTitleView.setText(c.getCaption());
                updateLeagueTable(c.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Set recyclerview layout and adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.addItemDecoration(new DividerLineDecoration(recyclerView.getContext()));
        recyclerView.setAdapter(adapter);

        // Populate competition dropdown
        Call<List<Competition>> compCall = competitionService.getCompetitions();
        compCall.enqueue(new Callback<List<Competition>>() {
            @Override
            public void onResponse(Call<List<Competition>> call, Response<List<Competition>> response) {
                List<Competition> competitions = response.body();
                spinnerList.clear();
                for (Competition c : competitions) {
                    spinnerList.add(c);
                }
                spinnerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Competition>> call, Throwable t) {
                leagueTitleView.setText(t.getMessage());
            }
        });
    }

    /**
     * Updates the league table RecyclerView with TeamStanding objects for given competition id
     *
     * @param competitionId Id of the competition
     */
    public void updateLeagueTable(int competitionId) {
        standingsList.clear();
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);

        Call<LeagueTableResponse> call = teamStandingService.getTeamStandings(competitionId);
        call.enqueue(new Callback<LeagueTableResponse>() {
            @Override
            public void onResponse(Call<LeagueTableResponse> call,
                                   Response<LeagueTableResponse> response) {

                LeagueTableResponse leagueTableResponse = response.body();
                List<TeamStanding> list = leagueTableResponse.getStandings();
                for (TeamStanding s : list) {
                    standingsList.add(s);
                }
                progressBar.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<LeagueTableResponse> call, Throwable t) {
                Log.d("OMADEBUG", t.getMessage());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void sortByHeader(View v) {
        TableHeaderView tv = (TableHeaderView) v;
        String text = tv.getText().toString();

        TableHeaderView.SortOrder currentOrder = tv.getSortOrder();

        switch (text) {
            case "#":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getPosition(), t2.getPosition()));
                break;
            case "Team name":
                Collections.sort(standingsList,
                        (t1, t2) -> t1.getTeamName().compareTo(t2.getTeamName()));
                break;
            case "G":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getPlayedGames(), t2.getPlayedGames()));
                break;
            case "W":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getWins(), t2.getWins()));
                break;
            case "D":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getDraws(), t2.getDraws()));
                break;
            case "L":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getLosses(), t2.getLosses()));
                break;
            case "P":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getPoints(), t2.getPoints()));
                break;
            case "GF":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getGoals(), t2.getGoals()));
                break;
            case "GA":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getGoalsAgainst(), t2.getGoalsAgainst()));
                break;
            case "GD":
                Collections.sort(standingsList,
                        (t1, t2) -> Integer.compare(t1.getGoalDifference(), t2.getGoalDifference()));
                break;
            default:
                Log.d("OMADEBUG", "Could not sort! invalid header");
        }

        resetHeaderIcons();

        Log.d("OMADEBUG", "Current order is: " + currentOrder);
        switch (currentOrder) {

            case ASCENDING:
                tv.setSortOrder(TableHeaderView.SortOrder.DESCENDING);
                break;
            case DESCENDING:
                Collections.reverse(standingsList);
                tv.setSortOrder(TableHeaderView.SortOrder.ASCENDING);
                break;
            case NONE:
                tv.setSortOrder(TableHeaderView.SortOrder.DESCENDING);
                break;
        }
        adapter.notifyDataSetChanged();
    }

    private void resetHeaderIcons() {
        RelativeLayout headers = findViewById(R.id.headerLayout);
        for (int i = 0; i < headers.getChildCount(); i++) {
            TableHeaderView v = (TableHeaderView) headers.getChildAt(i);
            v.setSortOrder(TableHeaderView.SortOrder.NONE);
        }
    }
}
