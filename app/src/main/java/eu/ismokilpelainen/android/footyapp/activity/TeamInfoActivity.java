package eu.ismokilpelainen.android.footyapp.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import eu.ismokilpelainen.android.footyapp.R;
import eu.ismokilpelainen.android.footyapp.fragment.FixtureListFragment;
import eu.ismokilpelainen.android.footyapp.fragment.PlayerListFragment;
import eu.ismokilpelainen.android.footyapp.model.Fixture;
import eu.ismokilpelainen.android.footyapp.model.Player;
import eu.ismokilpelainen.android.footyapp.model.Team;
import eu.ismokilpelainen.android.footyapp.service.ApiService;
import eu.ismokilpelainen.android.footyapp.service.TeamService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamInfoActivity extends AppCompatActivity implements FixtureListFragment.OnListFragmentInteractionListener,
        PlayerListFragment.OnListFragmentInteractionListener {

    private TeamService teamService = ApiService.getInstance()
            .getService(TeamService.class);

    private Team team;

    private FixtureListFragment fixtureListFragment;
    private PlayerListFragment playerListFragment;
    private PopupMenu sortMenu;

    private static final String KEY_FIXTURES = "eu.ismokilpelainen.android.footyapp.FIXTURES";
    private static final String KEY_PLAYERS = "eu.ismokilpelainen.android.footyapp.PLAYERS";

    private Fragment activeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_info);

        Button fixturesBtn = findViewById(R.id.fixturesTitle);
        Button playersBtn = findViewById(R.id.playersTitle);

        fixturesBtn.setOnClickListener((v) -> {
            setFragment(fixtureListFragment);
            fixturesBtn.setTextColor(Color.parseColor("#ff80cbc4"));
            playersBtn.setTextColor(getResources().getColor(android.R.color.white));
        });

        playersBtn.setOnClickListener(v->{
            setFragment(playerListFragment);
            playersBtn.setTextColor(Color.parseColor("#ff80cbc4"));
            fixturesBtn.setTextColor(getResources().getColor(android.R.color.white));
        });

        Intent intent = getIntent();

        String teamUrl = intent.getExtras().getString(MainActivity.EXTRA_TEAM_URL);

        Call<Team> call = teamService.getTeam(teamUrl);

        call.enqueue(new Callback<Team>() {

            @Override
            public void onResponse(Call<Team> call, Response<Team> response) {
                team = response.body();
                updateViews();
                updateFragments();
                setFragment(fixtureListFragment);
            }

            @Override
            public void onFailure(Call<Team> call, Throwable t) {
                Log.d("OMADEBUG","Callback failed: " + t.getMessage());
            }
        });
    }

    private void setFragment(Fragment newFragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        if (activeFragment != null) {
            transaction.hide(activeFragment);
        }

        if (!newFragment.isAdded()) {
            transaction.replace(R.id.fragmentView,newFragment);
        } else {
            transaction.show(newFragment);
        }
        transaction.commit();
        activeFragment = newFragment;

    }

    private void updateFragments() {
        FragmentManager fm = getFragmentManager();
        fixtureListFragment = FixtureListFragment.newInstance(1,team.getFixturesLink());
        fm.beginTransaction().replace(R.id.fragmentView,fixtureListFragment).commit();
        playerListFragment = PlayerListFragment.newInstance(1,team.getPlayersLink());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void updateViews() {
        if (team == null) {
            return;
        }
        TextView teamNameView = findViewById(R.id.teamName);
        ImageView crestView = findViewById(R.id.crestView);
        teamNameView.setText(team.getName());
        if (team.getCrestUrl() != null) {
            String url = team.getCrestUrl();
            SvgLoader.pluck()
                    .with(this)
                    .load(url, crestView);
            crestView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onListFragmentInteraction(Player item) {
        Log.d("OMADEBUG","testi");
    }

    @Override
    public void onListFragmentInteraction(Fixture item) {

    }

    public void showSortMenu(View v) {
        if (sortMenu == null) {
            Map<String, Runnable> sortMethods = fixtureListFragment.getSortMethods();

            sortMenu = new PopupMenu(this, v);
            MenuInflater inflater = sortMenu.getMenuInflater();
            inflater.inflate(R.menu.fixture_sort_menu, sortMenu.getMenu());
            Menu menu = sortMenu.getMenu();
            for (Map.Entry<String, Runnable> entry : sortMethods.entrySet()) {
                menu.add(R.id.sort_menu, View.generateViewId(), Menu.NONE, entry.getKey());
                sortMenu.setOnMenuItemClickListener((item) -> {
                    sortMethods.get(item.getTitle()).run();
                    item.setChecked(true);
                    return true;
                });
            }
            menu.setGroupCheckable(R.id.sort_menu, true, true);
            menu.getItem(0).setChecked(true);
        }

        sortMenu.show();
    }
}
