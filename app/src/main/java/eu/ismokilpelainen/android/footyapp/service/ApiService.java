package eu.ismokilpelainen.android.footyapp.service;

import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * A singleton class for creating Retrofit services
 */
public class ApiService {

    private static final String BASE_URL = "http://api.football-data.org/v1/";

    private static ApiService instance;

    private Retrofit retrofit;

    private Map<Class,Object> services = new HashMap<>();

    /**
     *  Private constructor to ensure only single instance is created
     */
    private ApiService() {

        // Initialize okhttp3 client builder with auth interceptor and header logging interceptor
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.addInterceptor(new AuthorizationInterceptor());
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        clientBuilder.addInterceptor(interceptor);

        // Inititialize Retrofit instance with Jackson Converters and customised client
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(clientBuilder.build())
                .build();
    }

    /**
     * Gets the ApiService singleton
     * @return ApiService instance
     */
    public static ApiService getInstance() {
        if (instance == null) {
            instance = new ApiService();
        }
        return instance;
    }

    /**
     * Returns service for requested class
     * @param serviceClass Service interface class
     * @return Instance of the service
     */
    @SuppressWarnings("unchecked")
    public <T> T getService (Class <T> serviceClass) {
        if (serviceClass == null) {
            throw new IllegalArgumentException("Service class cannot be null");
        }
        Object service = services.get(serviceClass);

        if (service == null) {
            service = retrofit.create(serviceClass);
            services.put(serviceClass, service);
        }

        return (T) service;
    }
}

