package eu.ismokilpelainen.android.footyapp.service;

import java.util.List;

import eu.ismokilpelainen.android.footyapp.model.Competition;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface CompetitionService {

    /*@Headers({"X-Auth-Token: 9a6f9f1d61844b7c87590b7198a05bca", "X-Response-Control: full"})*/
    @GET("competitions/")
    public Call<List<Competition>> getCompetitions();
}
