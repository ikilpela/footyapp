package eu.ismokilpelainen.android.footyapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashMap;

import eu.ismokilpelainen.android.footyapp.converter.LinksDeserializer;

@JsonIgnoreProperties("squadMarketValue")
public class Team {

    @JsonProperty
    private String name;
    @JsonProperty
    private String code;
    @JsonProperty
    private String shortName;
    @JsonProperty("_links")
    @JsonDeserialize(using = LinksDeserializer.class)
    private HashMap<String, String> links;
    @JsonProperty
    private String crestUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public void setCrestUrl(String crestUrl) {
        this.crestUrl = crestUrl;
    }

    public String getFixturesLink() {
        return links.get("fixtures");
    }

    public String getPlayersLink() {
        return links.get("players");
    }
}
