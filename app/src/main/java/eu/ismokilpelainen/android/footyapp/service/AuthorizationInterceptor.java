package eu.ismokilpelainen.android.footyapp.service;

import android.content.res.Resources;

import java.io.IOException;

import eu.ismokilpelainen.android.footyapp.R;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthorizationInterceptor implements Interceptor {

    private static final String API_KEY = "9a6f9f1d61844b7c87590b7198a05bca";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request newRequest = request.newBuilder()
                .addHeader("X-Auth-Token", API_KEY)
                .addHeader("X-Response-Control","full")
                .build();
        Response response = chain.proceed(newRequest);
        return response;
    }
}
