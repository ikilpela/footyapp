package eu.ismokilpelainen.android.footyapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eu.ismokilpelainen.android.footyapp.R;
import eu.ismokilpelainen.android.footyapp.fragment.PlayerListFragment.OnListFragmentInteractionListener;
import eu.ismokilpelainen.android.footyapp.model.Player;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Player} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class PlayerRecyclerViewAdapter extends RecyclerView.Adapter<PlayerRecyclerViewAdapter.ViewHolder> {

    private final List<Player> playerList;
    private final OnListFragmentInteractionListener mListener;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public PlayerRecyclerViewAdapter(List<Player> items, OnListFragmentInteractionListener listener) {
        playerList = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_player, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Player player = playerList.get(position);
        holder.mItem = player;
        holder.mPlayerNumberView.setText(String.valueOf(player.getNumber()));
        holder.mPlayerNameView.setText(player.getName());
        holder.mPlayerPositionView.setText(player.getPosition().toString());
        holder.mPlayerBirthdateView.setText(dateFormat.format(player.getDateOfBirth())
                + " ("
                + eu.ismokilpelainen.android.footyapp.DateUtils.getYearsFromDateToNow(player.getDateOfBirth())
                + " years)");

        holder.mPlayerNationalityView.setText(player.getNationality());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mPlayerNumberView;
        public final TextView mPlayerNameView;
        public final TextView mPlayerPositionView;
        public final TextView mPlayerNationalityView;
        public final TextView mPlayerBirthdateView;

        public Player mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPlayerNumberView = (TextView) view.findViewById(R.id.playerNumber);
            mPlayerNameView = (TextView) view.findViewById(R.id.playerName);
            mPlayerPositionView = (TextView) view.findViewById(R.id.playerPosition);
            mPlayerNationalityView = (TextView) view.findViewById(R.id.playerNationality);
            mPlayerBirthdateView = (TextView) view.findViewById(R.id.playerBirthdate);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mPlayerNameView.getText() + "'";
        }
    }
}
