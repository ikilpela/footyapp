package eu.ismokilpelainen.android.footyapp.service;

import eu.ismokilpelainen.android.footyapp.model.response.PlayersResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface PlayersService {

    @GET
    public Call<PlayersResponse> getPlayers(@Url String url);

    @GET("teams/{id}/players")
    public Call<PlayersResponse> getPlayers(@Path("id") Integer teamId);
}
