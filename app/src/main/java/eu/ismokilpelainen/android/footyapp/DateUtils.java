package eu.ismokilpelainen.android.footyapp;

import android.icu.util.TimeUnit;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static Calendar getCalendar(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public static int getYearsFromDateToNow(Date date) {
        long millis = System.currentTimeMillis() - date.getTime();
        int years = (int)Math.floor(millis / android.text.format.DateUtils.YEAR_IN_MILLIS);
        return years;
    }
}
