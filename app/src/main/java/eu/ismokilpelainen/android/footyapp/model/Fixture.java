package eu.ismokilpelainen.android.footyapp.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import eu.ismokilpelainen.android.footyapp.converter.LinksDeserializer;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "_links",
        "date",
        "status",
        "matchday",
        "homeTeamName",
        "awayTeamName",
        "result",
        "odds"
})
public class Fixture {

    public enum Status {
        TIMED,
        FINISHED;
    }

    @JsonProperty("_links")
    @JsonDeserialize(using = LinksDeserializer.class)
    private Map<String, String> links;
    @JsonProperty("date")
    private Date date;
    @JsonProperty("status")
    private Status status;
    @JsonProperty("matchday")
    private int matchday;
    @JsonProperty("homeTeamName")
    private String homeTeamName;
    @JsonProperty("awayTeamName")
    private String awayTeamName;
    @JsonProperty("result")
    private Result result;
    @JsonProperty("odds")
    private Object odds;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("_links")
    public Map<String, String> getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Map<String, String> links) {
        this.links = links;
    }

    @JsonProperty("date")
    public Date getDate() {
        return date;
    }

    @JsonProperty("date")
    public void setDate(Date date) {
        this.date = date;
    }

    @JsonProperty("status")
    public Status getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Status status) {
        this.status = status;
    }

    @JsonProperty("matchday")
    public int getMatchday() {
        return matchday;
    }

    @JsonProperty("matchday")
    public void setMatchday(int matchday) {
        this.matchday = matchday;
    }

    @JsonProperty("homeTeamName")
    public String getHomeTeamName() {
        return homeTeamName;
    }

    @JsonProperty("homeTeamName")
    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    @JsonProperty("awayTeamName")
    public String getAwayTeamName() {
        return awayTeamName;
    }

    @JsonProperty("awayTeamName")
    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    @JsonProperty("result")
    public Result getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(Result result) {
        this.result = result;
    }

    @JsonProperty("odds")
    public Object getOdds() {
        return odds;
    }

    @JsonProperty("odds")
    public void setOdds(Object odds) {
        this.odds = odds;
    }
}