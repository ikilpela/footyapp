package eu.ismokilpelainen.android.footyapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import eu.ismokilpelainen.android.footyapp.R;
import eu.ismokilpelainen.android.footyapp.activity.MainActivity;
import eu.ismokilpelainen.android.footyapp.activity.TeamInfoActivity;
import eu.ismokilpelainen.android.footyapp.model.TeamStanding;

public class StandingsAdapter extends RecyclerView.Adapter<StandingsAdapter.MyViewHolder> {

    private List<TeamStanding> standingsList;
    private final Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView standing, teamName, points, gamesPlayed, wins, ties, losses,
                goalsFor, goalsAgainst, goalDifference;

        public MyViewHolder(View view) {
            super(view);
            standing =  view.findViewById(R.id.standing);
            teamName =  view.findViewById(R.id.teamName);
            points =  view.findViewById(R.id.points);
            gamesPlayed = view.findViewById(R.id.gamesPlayed);
            wins = view.findViewById(R.id.wins);
            ties = view.findViewById(R.id.ties);
            losses = view.findViewById(R.id.losses);

            int orientation = context.getResources().getConfiguration().orientation;

            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                goalsFor = view.findViewById(R.id.goalsFor);
                goalsAgainst = view.findViewById(R.id.goalsAgainst);
                goalDifference = view.findViewById(R.id.goalDifference);
            }
        }
    }

    public StandingsAdapter(List<TeamStanding> standingsList, Context context) {
        this.standingsList = standingsList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.standings_row, parent, false);
        return new MyViewHolder(row);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final TeamStanding s = standingsList.get(position);
        holder.standing.setText(String.valueOf(s.getPosition()));
        holder.teamName.setText(s.getTeamName());
        holder.points.setText(String.valueOf(s.getPoints()));
        holder.gamesPlayed.setText(String.valueOf(s.getPlayedGames()));
        holder.wins.setText(String.valueOf(s.getWins()));
        holder.ties.setText(String.valueOf(s.getDraws()));
        holder.losses.setText(String.valueOf(s.getLosses()));
        if (holder.goalsFor != null) {
            holder.goalsFor.setText(String.valueOf(s.getGoals()));
        }
        if (holder.goalsAgainst != null) {
            holder.goalsAgainst.setText(String.valueOf(s.getGoalsAgainst()));
        }
        if (holder.goalDifference != null) {
            holder.goalDifference.setText(String.valueOf(s.getGoalDifference()));
        }

        holder.teamName.setOnClickListener((v) -> {
            Intent intent = new Intent(context, TeamInfoActivity.class);
            intent.putExtra(MainActivity.EXTRA_TEAM_URL, s.getTeamLink());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return standingsList.size();
    }

}
