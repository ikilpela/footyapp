package eu.ismokilpelainen.android.footyapp.fragment;

import java.util.Map;

public interface SortableFragment {

    Map<String,Runnable> getSortMethods();

}
