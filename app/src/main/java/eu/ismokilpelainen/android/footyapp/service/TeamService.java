package eu.ismokilpelainen.android.footyapp.service;

import eu.ismokilpelainen.android.footyapp.model.Team;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface TeamService {

    @GET
    public Call<Team> getTeam(@Url String url);
}
