package eu.ismokilpelainen.android.footyapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TeamRecord {

    @JsonProperty private int goals;
    @JsonProperty private int goalsAgainst;
    @JsonProperty private int wins;
    @JsonProperty private int draws;
    @JsonProperty private int losses;

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getDraws() {
        return draws;
    }

    public void setDraws(int ties) {
        this.draws = ties;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getGoalDifference() {
        return this.goals-this.goalsAgainst;
    }
}
