package eu.ismokilpelainen.android.footyapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.ismokilpelainen.android.footyapp.R;
import eu.ismokilpelainen.android.footyapp.decorator.DividerLineDecoration;
import eu.ismokilpelainen.android.footyapp.model.Fixture;
import eu.ismokilpelainen.android.footyapp.model.response.FixtureResponse;
import eu.ismokilpelainen.android.footyapp.service.ApiService;
import eu.ismokilpelainen.android.footyapp.service.FixtureService;
import eu.ismokilpelainen.android.footyapp.adapter.FixtureRecyclerViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class FixtureListFragment extends Fragment implements SortableFragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_API_URL = "url";

    // TODO: Customize parameters
    private int mColumnCount = 1;
    private String mApiUrl;

    private List<Fixture> fixtures = new ArrayList<>();
    private RecyclerView recyclerView;
    private ProgressBar loadingBar;
    private TextView errorView;

    private FixtureService fixtureService = ApiService.getInstance()
            .getService(FixtureService.class);

    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FixtureListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static FixtureListFragment newInstance(int columnCount, String url) {
        FixtureListFragment fragment = new FixtureListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(ARG_API_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            mApiUrl = getArguments().getString(ARG_API_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fixture_list, container, false);

        // Set the adapter

        Context context = view.getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        errorView = (TextView) view.findViewById(R.id.errorText);
        loadingBar = view.findViewById(R.id.loadingBar);
        recyclerView.addItemDecoration(new DividerLineDecoration(this.getActivity()));
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        recyclerView.setAdapter(new FixtureRecyclerViewAdapter(fixtures, mListener));
        if (mApiUrl != null && mApiUrl.length() > 0) {
            updateFixtureListAsync(mApiUrl);
        }

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
/*            String url = getArguments().getString(ARG_API_URL);
            updateFixtureListAsync(url);*/
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void updateFixtureListAsync(String url) {
        fixtures.clear();
        recyclerView.getAdapter().notifyDataSetChanged();
        loadingBar.setVisibility(View.VISIBLE);
        Call<FixtureResponse> call = fixtureService.getFixtures(url);
        call.enqueue(new Callback<FixtureResponse>() {
            @Override
            public void onResponse(Call<FixtureResponse> call, Response<FixtureResponse> response) {
                List<Fixture> newFixtures = response.body().getFixtures();
                for (Fixture f : newFixtures) {
                    fixtures.add(f);
                }
                recyclerView.getAdapter().notifyDataSetChanged();

                if (fixtures.isEmpty()) {
                    errorView.setVisibility(View.VISIBLE);
                    errorView.setText(getString(R.string.list_no_items_to_show));
                }
                loadingBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<FixtureResponse> call, Throwable t) {
                errorView.setVisibility(View.VISIBLE);
                errorView.setText(getString(R.string.list_error_retrieving_items));
                Log.e("OMADEBUG", t.getMessage());
                loadingBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public Map<String, Runnable> getSortMethods() {
        HashMap<String,Runnable> map = new HashMap<>();
        map.put("By date",this::sortByDate);
        map.put("By home goals",this::sortByHomeGoals);
        map.put("By away goals",this::sortByAwayGoals);
        return map;
    }

    public void sortByDate() {
        Collections.sort(fixtures,
                (o1, o2) ->  o1.getDate().compareTo(o2.getDate()));
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    public void sortByHomeGoals() {
        Collections.sort(fixtures,
                (o1,o2) -> Integer.compare(o1.getResult().getGoalsHomeTeam(),o2.getResult().getGoalsHomeTeam()));
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    public void sortByAwayGoals() {
        Collections.sort(fixtures,
                (o1,o2) -> Integer.compare(o1.getResult().getGoalsAwayTeam(),o2.getResult().getGoalsAwayTeam()));
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Fixture item);
    }
}
