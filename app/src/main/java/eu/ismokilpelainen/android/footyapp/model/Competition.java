package eu.ismokilpelainen.android.footyapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(value = {"_links"})
@JsonPropertyOrder({
        "id",
        "caption",
        "league",
        "year",
        "currentMatchday",
        "numberOfMatchdays",
        "numberOfTeams",
        "numberOfGames",
        "lastUpdated"
})
public class Competition {

    @JsonProperty("id")
    private int id;
    @JsonProperty("caption")
    private String caption;
    @JsonProperty("league")
    private String league;
    @JsonProperty("year")
    private String year;
    @JsonProperty("currentMatchday")
    private int currentMatchday;
    @JsonProperty("numberOfMatchdays")
    private int numberOfMatchdays;
    @JsonProperty("numberOfTeams")
    private int numberOfTeams;
    @JsonProperty("numberOfGames")
    private int numberOfGames;
    @JsonProperty("lastUpdated")
    private String lastUpdated;


    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("caption")
    public String getCaption() {
        return caption;
    }

    @JsonProperty("caption")
    public void setCaption(String caption) {
        this.caption = caption;
    }

    @JsonProperty("league")
    public String getLeague() {
        return league;
    }

    @JsonProperty("league")
    public void setLeague(String league) {
        this.league = league;
    }

    @JsonProperty("year")
    public String getYear() {
        return year;
    }

    @JsonProperty("year")
    public void setYear(String year) {
        this.year = year;
    }

    @JsonProperty("currentMatchday")
    public int getCurrentMatchday() {
        return currentMatchday;
    }

    @JsonProperty("currentMatchday")
    public void setCurrentMatchday(int currentMatchday) {
        this.currentMatchday = currentMatchday;
    }

    @JsonProperty("numberOfMatchdays")
    public int getNumberOfMatchdays() {
        return numberOfMatchdays;
    }

    @JsonProperty("numberOfMatchdays")
    public void setNumberOfMatchdays(int numberOfMatchdays) {
        this.numberOfMatchdays = numberOfMatchdays;
    }

    @JsonProperty("numberOfTeams")
    public int getNumberOfTeams() {
        return numberOfTeams;
    }

    @JsonProperty("numberOfTeams")
    public void setNumberOfTeams(int numberOfTeams) {
        this.numberOfTeams = numberOfTeams;
    }

    @JsonProperty("numberOfGames")
    public int getNumberOfGames() {
        return numberOfGames;
    }

    @JsonProperty("numberOfGames")
    public void setNumberOfGames(int numberOfGames) {
        this.numberOfGames = numberOfGames;
    }

    @JsonProperty("lastUpdated")
    public String getLastUpdated() {
        return lastUpdated;
    }

    @JsonProperty("lastUpdated")
    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return this.caption;

    }
}
