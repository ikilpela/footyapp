package eu.ismokilpelainen.android.footyapp.model.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import eu.ismokilpelainen.android.footyapp.model.TeamStanding;

@JsonIgnoreProperties(value={"_links"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "leagueCaption",
        "matchday",
        "standing"
})
public class LeagueTableResponse {

    @JsonProperty("_links")
    private String _links;

    @JsonProperty("leagueCaption")
    private String leagueCaption;
    @JsonProperty("matchday")
    private int matchday;
    @JsonProperty("standing")
    private List<TeamStanding> standings;

    @JsonProperty("leagueCaption")
    public String getLeagueCaption() {
        return leagueCaption;
    }

    @JsonProperty("leagueCaption")
    public void setLeagueCaption(String leagueCaption) {
        this.leagueCaption = leagueCaption;
    }

    @JsonProperty("matchday")
    public int getMatchday() {
        return matchday;
    }

    @JsonProperty("matchday")
    public void setMatchday(int matchday) {
        this.matchday = matchday;
    }

    @JsonProperty("standing")
    public List<TeamStanding> getStandings() {
        return standings != null ? standings : new ArrayList<TeamStanding>();
    }

    @JsonProperty("standing")
    public void setStandings(List<TeamStanding> standings) {
        this.standings = standings;
    }

    public String get_links() {
        return _links;
    }

    public void set_links(String _links) {
        this._links = _links;
    }

}
