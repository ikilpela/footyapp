package eu.ismokilpelainen.android.footyapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import eu.ismokilpelainen.android.footyapp.R;
import eu.ismokilpelainen.android.footyapp.adapter.PlayerRecyclerViewAdapter;
import eu.ismokilpelainen.android.footyapp.decorator.DividerLineDecoration;
import eu.ismokilpelainen.android.footyapp.model.Player;
import eu.ismokilpelainen.android.footyapp.model.response.PlayersResponse;
import eu.ismokilpelainen.android.footyapp.service.ApiService;
import eu.ismokilpelainen.android.footyapp.service.PlayersService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PlayerListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_API_URL = "url";

    private int mColumnCount = 1;
    private String mApiUrl;

    private PlayersService playersService = ApiService.getInstance()
            .getService(PlayersService.class);
    private List<Player> players = new ArrayList<>();
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PlayerListFragment() {
    }


    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PlayerListFragment newInstance(int columnCount, String url) {
        PlayerListFragment fragment = new PlayerListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        args.putString(ARG_API_URL,url);
        fragment.setArguments(args);
        return fragment;
    }

    public void updatePlayerListAsync(String url) {
        players.clear();
        recyclerView.getAdapter().notifyDataSetChanged();
        Call<PlayersResponse> call = playersService.getPlayers(url);
        call.enqueue(new Callback<PlayersResponse>() {
            @Override
            public void onResponse(Call<PlayersResponse> call, Response<PlayersResponse> response) {
                List<Player> newPlayers = response.body().getPlayers();
                Collections.sort(newPlayers,
                        (Player p1, Player p2)
                                -> {return p1.getPosition().compareTo(p2.getPosition());});

                for(Player p : newPlayers) {
                    players.add(p);
                }
                recyclerView.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<PlayersResponse> call, Throwable t) {
                Log.e("OMADEBUG", t.getMessage());
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
            mApiUrl = getArguments().getString(ARG_API_URL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.addItemDecoration(new DividerLineDecoration(this.getActivity()));
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new PlayerRecyclerViewAdapter(players, mListener));
            if (mApiUrl != null && mApiUrl.length() > 0) {
                updatePlayerListAsync(mApiUrl);
            }
        }
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Player item);
    }
}
