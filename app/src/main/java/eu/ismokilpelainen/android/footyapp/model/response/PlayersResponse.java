package eu.ismokilpelainen.android.footyapp.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashMap;
import java.util.List;

import eu.ismokilpelainen.android.footyapp.converter.LinksDeserializer;
import eu.ismokilpelainen.android.footyapp.model.Player;

public class PlayersResponse {

    @JsonProperty("_links")
    @JsonDeserialize(using = LinksDeserializer.class)
    private HashMap<String,String> links;

    @JsonProperty()
    private int count;

    private List<Player> players;

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
