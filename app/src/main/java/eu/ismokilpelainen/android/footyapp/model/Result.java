package eu.ismokilpelainen.android.footyapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Result {

    @JsonProperty("goalsHomeTeam")
    private int goalsHomeTeam;
    @JsonProperty("goalsAwayTeam")
    private int goalsAwayTeam;

    @JsonProperty("halfTime")
    private Result halfTimeResult;

    public int getGoalsHomeTeam() {
        return goalsHomeTeam;
    }

    public void setGoalsHomeTeam(int goalsHomeTeam) {
        this.goalsHomeTeam = goalsHomeTeam;
    }

    public int getGoalsAwayTeam() {
        return goalsAwayTeam;
    }

    public void setGoalsAwayTeam(int goalsAwayTeam) {
        this.goalsAwayTeam = goalsAwayTeam;
    }

    public Result getHalfTimeResult() {
        return halfTimeResult;
    }

    public void setHalfTimeResult(Result halfTimeResult) {
        this.halfTimeResult = halfTimeResult;
    }

    @Override
    public String toString() {
        String str = goalsHomeTeam
                + " - "
                + goalsAwayTeam;
/*        if (halfTimeResult != null) {
            str += " (" + halfTimeResult.toString() + ")";
        }*/
        return str;
    }
}
