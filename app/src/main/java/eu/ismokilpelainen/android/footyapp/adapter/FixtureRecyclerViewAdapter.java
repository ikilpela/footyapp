package eu.ismokilpelainen.android.footyapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eu.ismokilpelainen.android.footyapp.R;
import eu.ismokilpelainen.android.footyapp.fragment.FixtureListFragment.OnListFragmentInteractionListener;
import eu.ismokilpelainen.android.footyapp.model.Fixture;
import eu.ismokilpelainen.android.footyapp.model.Result;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Fixture} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class FixtureRecyclerViewAdapter extends RecyclerView.Adapter<FixtureRecyclerViewAdapter.ViewHolder> {

    private final List<Fixture> mValues;
    private final OnListFragmentInteractionListener mListener;

    public FixtureRecyclerViewAdapter(List<Fixture> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_fixture, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        Fixture f = mValues.get(position);

        String homeTeam = f.getHomeTeamName();
/*        if (homeTeam.length() > 15 ) {
            homeTeam = homeTeam.substring(0,13).concat("..");
        }*/

        String awayTeam = f.getAwayTeamName();
/*        if (awayTeam.length() > 15 ) {
            awayTeam = awayTeam.substring(0,13).concat("..");
        }*/

        holder.homeTeamView.setText(homeTeam);
        holder.awayTeamView.setText(awayTeam);
        Result result = f.getResult();
        if (result != null) {
            holder.scoreView.setText(f.getResult().toString());
            if (result.getHalfTimeResult() != null) {
                holder.scoreHTView.setText("(" + f.getResult().getHalfTimeResult().toString() + ")");
            }
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView homeTeamView;
        public final TextView awayTeamView;
        public final TextView scoreView;
        public final TextView scoreHTView;
        
        public Fixture mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            homeTeamView = (TextView) view.findViewById(R.id.homeTeam);
            awayTeamView = (TextView) view.findViewById(R.id.awayTeam);
            scoreView = (TextView) view.findViewById(R.id.score);
            scoreHTView = (TextView) view.findViewById(R.id.scoreHT);
        }

    }
}
