package eu.ismokilpelainen.android.footyapp.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.HashMap;
import java.util.List;

import eu.ismokilpelainen.android.footyapp.converter.LinksDeserializer;
import eu.ismokilpelainen.android.footyapp.model.Fixture;

public class FixtureResponse {

    @JsonProperty("_links")
    @JsonDeserialize(using = LinksDeserializer.class)
    private HashMap<String,String> links;

    @JsonProperty
    private String season;

    @JsonProperty
    private int count;

    @JsonProperty
    private List<Fixture> fixtures;

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Fixture> getFixtures() {
        return fixtures;
    }

    public void setFixtures(List<Fixture> fixtures) {
        this.fixtures = fixtures;
    }
}
