package eu.ismokilpelainen.android.footyapp.model;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;
import java.util.Map;

public enum PlayerPosition implements Comparable<PlayerPosition> {

    KEEPER(0,PositionGroup.GOALKEEPER),
    CENTREBACK(1,PositionGroup.DEFENCE),
    LEFTBACK(2,PositionGroup.DEFENCE),
    RIGHTBACK(3,PositionGroup.DEFENCE),
    DEFENSIVEMIDFIELDER(4,PositionGroup.MIDFIELD),
    CENTRALMIDFIELDER(5,PositionGroup.MIDFIELD),
    LEFTMIDFIELDER(6,PositionGroup.MIDFIELD),
    RIGHTMIDFIELDER(7,PositionGroup.MIDFIELD),
    ATTACKINGMIDFIELDER(8,PositionGroup.MIDFIELD),
    CENTREFORWARD(9,PositionGroup.ATTACK),
    SECONDARYSTRIKER(10,PositionGroup.ATTACK),
    LEFTWING(11,PositionGroup.ATTACK),
    RIGHTWING(12,PositionGroup.ATTACK);

    private PositionGroup group;
    private int number;

    private static Map<String, PlayerPosition> jsonMappings = new HashMap<>();

    static {
        jsonMappings.put("keeper", PlayerPosition.KEEPER);
        jsonMappings.put("left-back", PlayerPosition.LEFTBACK);
        jsonMappings.put("right-back", PlayerPosition.RIGHTBACK);
        jsonMappings.put("centre-back", PlayerPosition.CENTREBACK);
        jsonMappings.put("defensive midfield", PlayerPosition.DEFENSIVEMIDFIELDER);
        jsonMappings.put("central midfield", PlayerPosition.CENTRALMIDFIELDER);
        jsonMappings.put("left midfield", PlayerPosition.LEFTMIDFIELDER);
        jsonMappings.put("right midfield", PlayerPosition.RIGHTMIDFIELDER);
        jsonMappings.put("attacking midfield", PlayerPosition.ATTACKINGMIDFIELDER);
        jsonMappings.put("left wing", PlayerPosition.LEFTWING);
        jsonMappings.put("right wing", PlayerPosition.RIGHTWING);
        jsonMappings.put("secondary striker", PlayerPosition.SECONDARYSTRIKER);
        jsonMappings.put("centre-forward", PlayerPosition.CENTREFORWARD);
    }

    PlayerPosition(int number, PositionGroup group) {
        this.number = number;
        this.group = group;
    }

    @JsonCreator
    public static PlayerPosition forValue(String value) {
        return jsonMappings.get(value.toLowerCase());
    }

    public enum PositionGroup {GOALKEEPER, DEFENCE, MIDFIELD, ATTACK}

    public PositionGroup getPositionGroup() {
        return this.group;
    }

    public String getShort() {
        switch(this) {
            case KEEPER:
                return "GK";
            case LEFTBACK:
                return "LB";
            case RIGHTBACK:
                return "RB";
            case CENTREBACK:
                return "CB";
            case DEFENSIVEMIDFIELDER:
                return "DM";
            case CENTRALMIDFIELDER:
                return "CM";
            case LEFTMIDFIELDER:
                return "LM";
            case RIGHTMIDFIELDER:
                return "RM";
            case ATTACKINGMIDFIELDER:
                return "AM";
            case LEFTWING:
                return "LW";
            case RIGHTWING:
                return "RW";
            case SECONDARYSTRIKER:
                return "SS";
            case CENTREFORWARD:
                return "CF";
        }
        return "";
    }

    @Override
    public String toString() {
        switch (this) {
            case KEEPER:
                return "Goalkeeper";
            case LEFTBACK:
                return "Left Defender";
            case RIGHTBACK:
                return "Right Defender";
            case CENTREBACK:
                return "Central Defender";
            case DEFENSIVEMIDFIELDER:
                return "Defensive Midfielder";
            case CENTRALMIDFIELDER:
                return "Central Midfielder";
            case LEFTMIDFIELDER:
                return "Left Midfielder";
            case RIGHTMIDFIELDER:
                return "Right midfielder";
            case ATTACKINGMIDFIELDER:
                return "Attacking Midfielder";
            case LEFTWING:
                return "Left Winger";
            case RIGHTWING:
                return "Right Winger";
            case SECONDARYSTRIKER:
                return "Secondary Striker";
            case CENTREFORWARD:
                return "Central Forward";
        }
        return "";
    }
}
